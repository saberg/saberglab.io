---
layout: post
title:  "Fixing a dodgy Keithley 2000 DMM (part 1)"
date:   2020-11-03 00:00:00 -0000
categories: blog
---

A few years back, I acquired a used but broken Keithley 2000. As it showed some errors when doing a self test, I've replaced all capacitors, which are prone to leaking after years of use. This fixed the self test errors.

As a next step, I've upgraded the firmware from A12 to A19 (see https://xdevs.com/fix/kei2000/#firmware). The guide is pretty straight forward. Be vary of installing the A20 version, as it will destroy the calibration data of a separate flash IC.

Unfortunately, there were still some issues left. The front/back switch was pretty mushy and 4 wire measurements showed around -11kOhm when shorting all the connectors. The input selector switch is the first thing after the connectors themself. Measuring the resistance between each pole revealed a resistance of over 10Ohm. Not good in a precision instrument. 

Desoldering the switch was a bit tricky, as the pins are pretty big while the PCB holes are somewhat slim. But after some struggle, the switch came loose. Without a desoldering station, this part would have been borderline impossible.

Opening the switch was relatively easy. At first I've remove the metal clamp which holds the spring in place. After dislocating the C-clamp which contains the "click action" pin, the whole switch shaft could be pulled out at the back. The contact blades are spring tensioned, so they were flying out. And sure enough, there was some sticky residue on the contact blades. It looks like whatever lubricant Keithley used was solidifying.


![disassembling the switch](/assets/blog/2021-04-16-keithley_repair_p1/switch.jpg)

**Switch in the process of being disassembled.**


The residue came clean off with the help of some Isopropanol. I'm currently awaiting some silicone grease so I can put the switch back together.


![dirty vs clean](/assets/blog//2021-04-16-keithley_repair_p1/contacts.jpg)

**Clean vs dirty contacts.**