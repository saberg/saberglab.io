---
layout: post
title:  "Scrapped Kodi screensaver"
date:   2021-05-10 00:00:00 -0000
categories: blog
---

As you may know, Kodi 19 has the codename "Matrix". As a first test bed to learn about OpenGL/GLSL, I've written fragment shaders on shadertoy.com, an easy to use testing canvas. 

I've modified an existing shadertoy add-on of Kodi to be host of my newly made one: a Marix inspired music vizualization. After a lot of optimization, I've managed to get a visually pleasing viz going, while refraining from using "expensive" functions like square root or sine. Moving most of the static computations to the CPU, I've managed to run it at 50 fps on a Raspberry Pi 1!

<iframe width="640" height="360" frameborder="0" src="https://www.shadertoy.com/embed/WldBDf?gui=true&t=10&paused=true&muted=false" allowfullscreen></iframe>

**The Kodi Matrix visualization in Shadertoy. Press play so start.**

As the visualization took a lot of effort, I've newer found the time to get my "Code Rain" shader into a screensaver add-on. It features falling strings of letters, akin to the effect used on various monitors in the movie "Martix". The code needs some cleanup, but the effect is more or less finished.

<iframe width="640" height="360" frameborder="0" src="https://www.shadertoy.com/embed/tllfDB?gui=true&t=10&paused=true&muted=false" allowfullscreen></iframe>

**The unpublished "Code Rain" screensaver. Press play so start.**

It was my first try of using SDF textures (signed distance fields/functions, https://en.wikipedia.org/wiki/Signed_distance_function). The cool thing about them is the ability to stay sharp while being up close to the screen, while the texture is small. Its outline is almost vector-graphics like. Even better, it enabled me to implement a range of interesting effects. All the letters have an inner plus an outer glow, which is animated based on the distance. If the letters get to close to the screen, they fade out by blurring. 

The shader displayed above is interactive. You can drag your mouse over it to look around the realm.