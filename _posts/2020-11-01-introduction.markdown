---
layout: post
title:  "Introduction"
date:   2016-11-03 00:00:00 -0000
categories: blog
---


# Introduction

Hi, my name is Sascha Bergner, technician for industrial engineering and free-time software developer.

Over the last few years, I've gathered much insight in the innerworkings of lots of proprietary equipment by reversing and analyzing soft- and hardware. I've been planning to put my projects and ideas somewhere to display, but never got aroud to do so until now. The main subject of this blog resolves mainly around hardware hacking, with a large portion focusing on industrial equipment like PLC's and such. 

Besides this, I'm also an developer for the Kodi Media Center (https://kodi.tv/), with a focus on Python extensions and OpenGL rendering. Check out my Github account if this peaks your interest: https://github.com/sarbes/!
