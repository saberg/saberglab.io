---
layout: post
title:  "Getting screenshots off a Hantek DSO"
date:   2021-04-23 00:00:00 -0000
categories: blog
---

From an outside perspective, my blog is (was) a bit lacking of pictures conveying my work. Owning a Voltcraft DSO-3062C oscilloscope (a re-branded Hantek 5000), I could snap a few screenshots while I'm measuring - so much in theory. 


![dso 3062c](/assets/blog/2021-04-23-hantek_screenshots/oscilloscope.jpg)

**My Voltcraft DSO-3062C oscilloscope.**


The OEM software is very much lacking, as it is a low budget scope. Not only is there none for Linux, the Windows versions are getting rarer as download links cease to exist. 

In my searches of the web, I came across the very nicely made documentation of the USB protocol (https://www.mikrocontroller.net/articles/Hantek_Protokoll). It describes all possible command codes, including some more advanced ones (like writing to the FPGA). At the moment, I was mostly interested in the screenshot command. I decided to write a Python implementation, as I knew the language. To keep the program light, I was using just pyusb and pypng.

At first, a udev rule would have to be implemented. The one which worked on my system was: 'SUBSYSTEM=="usb", ACTION=="add", ENV{DEVTYPE}=="usb_device", ATTRS{idVendor}=="049f", ATTRS{idProduct}=="505a", TAG+="uaccess", TAG+="udev-acl"'.

Unfortunately, Hantek has copied the VID:PID of an SDK (the VID belongs to Compaq), so Manjaro would try to connect to a non-existing Network adapter. Luckily, this could be disabled instructing libusb to detach any kernel drivers.

A bit of trial and error later, I could receive echo commands from the device. Bulk USB communication is very straight forward. The protocol itself is not so much. It took some time to get a rudimentary response parser going.

After a bit of data massaging, I've downloaded and processed my first screenshot:


![grey screenshot](/assets/blog/2021-04-23-hantek_screenshots/grey.png)

**A corny looking greyscale screenshot.**


The image lacks colour, as the data is supposed to be palletized. The OEM software has the palette built in, but was not openly available. On some dodgy platforms, I've found a setup file which looked like it could be genuine. But I couldn't extract them on my Linux machine. Running the executable in Wine was out of the question for me, and spooling up a Windows VM would have taken too long. The firmware image also has the palette, but it is encrypted. But yet again, the smart wizards of the internet (https://elinux.org/Das_Oszi) provide the password ("0571tekway") to it. After searching in the unpacked binary, I've found what I was looking for.


![palette](/assets/blog/2021-04-23-hantek_screenshots/palette.png)

**The extracted palette.**


Pasting the palette into my script gave me the right results, after I've removed the alpha channel.


![colour screenshot](/assets/blog/2021-04-23-hantek_screenshots/colour.png)

**The proper palette of the image.**


The code is available at my repository at: https://gitlab.com/saberg/hantek-5000-python-library