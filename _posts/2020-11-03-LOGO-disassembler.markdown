---
layout: post
title:  "Starting small: LOGO!"
date:   2020-11-03 00:00:00 -0000
categories: blog
---

Some years ago, I've aquired a Siemens LOGO! 24RC. A small form factor PLC, suitable to do light tasks (like controlling a garage door). The base model without extensions features eight 24V DC inputs and four relay outputs. This variant has been succeeded by a more recent model with color LCD and networking options, but the Siemens IDE still features support for it (like any older variant).

The unit was pretty easy to open. It features a two board construction, one processing board and one for PSU/IO. Both are interconnected via a 2x19 pin header.

While the base unit featuring all of the IO is pretty standard, the processor PCB is more interesting. It features an ARM processor, two serial flash ROMs, a display and some connectors. As typical for Siemens products, there is no silkscreen on the PCB. Noteworthy were the amount of test points; with no, one or two probing marks on them (https://hackaday.com/2019/02/09/test-pcbs-on-a-bed-of-nails/).

Besides the normal programming connector, it also features an interesting 2x5 edge connector, normally unaccessable. 

In the future, I'll explore more of the inner workings.